public with sharing class AccountController { 
 //Get Account Data onload
    @AuraEnabled
    public static List< Account > getAccounts() { 
      
        return [ SELECT Id, Name, Industry FROM Account LIMIT 10 ]; 
         
    } 
  // Save Account Records.   
    @AuraEnabled
    public static void saveAccounts(List<Account> accList){
        Insert accList;
    } 
}