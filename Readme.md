# Dynamic Add/Delete Row #

### What is this repository for ?  
By using this repository we can add and delete rows dynamically on the user interface by simple clicks. 
It can be used where multiple rows are to be added in a list or to be deleted as per the requirement without hard coding the number of rows in the code.

### How to Use
For building this component we are going to create following components and controllers.

* 	DynamicallyAddDeleteRow (Lightning Web Component).
* AccountController (Apex Controller).
